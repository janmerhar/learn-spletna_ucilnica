# Spletna učilnica Learn

## Uporabniško ime in geslo
Uporabniško ime | Geslo
----------------|------
franch | 456
marikova | 789
markok | 654
**merjan** | **123**
mlakarivan | 987
novakj | 321
zupanivan | 654

**Skrbnik vseh učilnic**

## Gesla učilnic
Učilnica | Geslo
---------|------
Zaklenjena učilnica | 123

## Namestitev spletne aplikacije
1. Prenesite datoteko iz Gitlab-a.
1. Mapo **learn-spletna_ucilnica** kopirajte v **C:\xampp\htdocs** 
1. Datoteko **learn-spletna_ucilnica/baza/learn.sql** uvozite v MySQL.
1. Do spletne aplikacije se dostopa preko URL **localhost/learn-spletna_ucilnica/indeks.php**.